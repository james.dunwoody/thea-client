[< Back to main](../../README.md)

## Python Sample Code

This example makes use of python 3.9. Other versions of python will work with minor changes.

## Setup

```sh
python3.9 -m venv venv

source venv/bin/activate

python3.9 -m pip install --upgrade pip

pip install requests
```

## Running

Run classification and field extraction by calling the following python [script](sample.py).

```sh
python3.9 sample.py --file=FULL_PATH_TO_FILE.pdf --key=YOUR_THEA_API_KEY --thea_user_id=YOUR_THEA_USER_ID --thea_user_secret=YOUR_THEA_SECRET
```

[< Back to main](../../README.md)
